

Разный контент, связанный с темой.

Фактически, черновик.





### Черновик оглавления проекта.

Человеку свойственно мечтать. Мечтать о том о других мирах.
Кто-то мечтает о мире с волшебниками, кто-то - о мире, в котором живут разные лесные зверюшки.
Кто-то думает про мир будущего с инопланетянами, кто-то - про розовых пони.

И наконец есть миры, живущие по другим законам. Существует огромное количество статей про то, что будет, если бы мир был другим.


Вышышленные миры тем и хороши, что для них можно придумать любые законы.
В частности, как бы жилось в мире с неевклидовой геометрией?
Именно этому и посвящён проект.

Различия между геометриями довольно легко просчитываются на примере довольно простых ситуаций.

Например, достаточно хорошо известно, что радиус и длина окружности (при достаточном радиусе) меняется по экспоненте. при увеличении радиуса на единицу длина окружности и площадь круга увеличиваются в `e`(2.718) раз. Таким образом, 

Ещё одна особенность - возможность разделить пространство на половины таким образом, что их будет много.

Геометрия Лобачевского позволяет создавать конктрукции, невозможные в Евклидовой геометрии.
Как вам паркет из бесконечных треугольников?
А из бесконечноугольников, у которых все углы прямые?

Часть фактов легко гугялтся.
В hyperrogue есть огромное количество комбинаций, возможных только в геометрии Лобачевского.


Существую несколько игр, в которых можно ощутить геометрию Лобачевского.
Например, есть игра [hyperrogue](https://roguetemple.com/z/hyper/), в которой можно наглядно ощутить, насколько геометрия Лобачевского отличается от Евклидовой.




#### треугольник "Роршарха"

Интересный факт: из одной геометрической конструкции можно получить несколько задач.
Взять к примеру прямоугольный треугольник, у которого пара сторон бесконечны(сходятся в идеальной точке) - так называемый треугольник параллельности.
Соотношение между длиной отрезка и острым углом можно найти в [этой статье](https://ru.wikipedia.org/wiki/Угол_параллельности),

Если представить, что идеальная точка - звезда, то получаем задачу про Ёжика и звезду(пылится на яндекс диске аж с 2013 года), где отрезок - расстояние между точкой под звездой и самим Ёжиком, а угол - угол, на котором находится звезда.

Перевернём треугольник - получится задача про горизонт - описана в этой вики. Там отрезком будет расстояние между поверхностью и глазами наблюдателя(или камерой), а угол - угол относительно вертикального.

А можно положить треугольник полностью на поверхность или вообще перевернуть ногами - получится угловой размер половины пространства/плоскости/прямой. правда, углом у нас будет угловой радиус.






#### небольшой рассказик.

*написано 2020-10-14 под альбом [New Galaxy In Your Telescope](https://gv-sound.bandcamp.com/album/new-galaxy-in-your-telescope)*

*Дефис был скопирован с темы на google-группах.*


— Вот скажи..

— Что скажи?

— А ты подумай..

— Что подумай?

— Вот что было бы, если бы мир был другим?

— Другим?

— Да. Вот представь себе, летят две звезды. В одну сторону. И не расходятся.

— Да ну! Ведь все же знают, что если пустить две стрелы параллельно, то они разлетятся в разные стороны.

— Так это в нашем мире. А в том..

— Что в том?

— В том мире параллельные линии всегда держатся на одном расстоянии!

— Параллельные? Странное какое-то понятие. Кто-то называет параллельными расходящиеся линии, кто-то - сходящиеся к одной точке.

— Ага. А там они на одном расстоянии держатся.

— Как же так? На одном расстоянии? Все же знают, что чтобы держаться на одном расстоянии от прямой, надо немного подворачивать.

— Ага. Так это же у нас. У нас понятно, побежишь в одну сторону, потом окажется, что бежите в разные.

— А у них так просто держаться рядом.

— Ещё скажи, что у них звезды везде одни и те же.

— И это тоже. Одна звезда во всём мире видна.

— Да ты шутишь!

— Ага. Полярная. Всегда на севере.

— А что такое север?


...

##### .


— Смотри! - Вот там какое-то созвездие!

— Давай к нему побежим?

— Давай!





#### рандомные задачи

Просто идеи задач, которые надо бы решить и запостить.




###### разделение территории

Два игрока имеют свою территорию, причём территория растёт с постоянной скоростью.

При этом нельзя захватывать чужую территорию, только нейтральную.

Если игроки начинают с одной точки и растут с одинаковой скоростью, то они поделят пространсто пополам, причём границей будет прямая, равноудалённая от обоих точек.

Если один из игроков растёт чуть быстрее, то на Евклидовой плоскости более быстрый игрок рано или поздно окружит более медленного. Таким образом, второй игрок захватит только площадь конечного размера.

1. Чуть позже попробую вывести доказательство, что первый игрок всё-таки окружит второго.
2. Задача посложнее - рассчитать, какую именно форму примет территория второго игрока. Нутром чую, будет подобие яйца. Самому интересно рассчитать, но силов нет.

А теперь самое интересное - каково будет решение в геометрии Лобачевского.
До конца не уверен, но если более медленный игрок будет успеет накопить достаточно площади, то ему по-прежнему будет доступна бесконечная площадь.

Какую форму будет иметь граница? Вопрос интересный.

Как я пришёл к этой задаче? Вот такой сценарий: есть два вида, один размножается быстее(мыши какие-нибудь), другой размножается медленнее(зайцы, люди или медведи).
Медленные существа могут построить границу от мышей, чтобы оставить территорию себе.
В Евклидовом мире такая тактика не поможет - мыши их рано или поздно окружат.
В Лобачевском - они смогут расширяться до бесконечности.

План: можно(точнее, нужно) построить симуляцию данной задачи на разных плоскостях. можно взять какой-нибудь клеточный автомат и реализовать симуляцию там.
С Лобачевским вариантом сложнее, нужно либо поковырять движок hyperrogue, либо найти другую библиотеку. Надеюсь, у меня(либо у кого-то ещё) когда-нибудь дойдут до этого руки.


######










