hyperbolic walk, [~~походка Лобачевского~~](https://posmotre.li/Сам_себе_надмозг) ходьба с поворотами 


Каждый шаг наш путник проходит расстояние `l`, после чего поворачивает на угол $`\alpha`$

При этом путник движется по многоугольнику(но это не точно).

Многоугольник можно разбить на n равнобедренных треугольников, каждый из которых можно разбить на 2 прямоугольных треугольника.


В Евклидовом пространстве путник движется вокруг точки по многоугольнику.

Для каждого участка можно построить прямоугольный треугольник, соединив центр участка, точку поворота и центр многоугольника.

n-угольник можно разбить на 2n прямоугольных треугольников, при этом угол у поворота равен половине угла поворота.

Угол у треугольника равен
```math
A = \frac{\pi-\alpha}{2}
```

попробуем найти:
- `r` - радиус вписанной окружности
- `R` - радиус описанной окружности
- `n` - количество сторон у многоугольника

#### Евклидова геометрия

```math
\frac{\pi - \alpha}{2} + \frac{\pi}{n}=\frac{\pi}{2} \\
\frac{\pi}{n} = \frac{\alpha}{2} \\
n = \frac{2\pi}{\alpha} \\
r = \frac{l \cot{\frac{\alpha}{2}}}{2} \\
R = \frac{l}{2\sin{\frac{\alpha}{2}}}
```

В Евклидовой геометрии количество сторон зависит только от угла поворота, а размер многоугольника прямо пропорционален расстоянию участка при заданном угле.



### Геометрия Лобачевского
```math
\cos{\frac{\pi}{n}} = \cosh{\frac{a}{2}}\sin{A} \\
\tanh{r} = \sinh{\frac{a}{2}}\tan{A} \\
\tanh{R} = \frac{\tanh{\frac{a}{2}}}{\cos{A}} \\
```
При этом если
```math
\cosh{\frac{a}{2}}\sin{A} > 1
```
то путник будет двигаться вдоль прямой, где каждый участок будет Четырёхугольником Саккери.

Четырёхугольником Саккери можно разбить на 2 четырёхугольника Ламберта, такие, что
```math
AF=\frac{l}{2}, \ \angle AFB = A = \frac{\pi-\alpha}{2}, OB = \frac{l'}{2}, \\
r = OA, \ R = BF
```

```math
\sin{A} = \frac{\cosh \frac{l'}{2}}{\cosh \frac{l}{2}}  \\
\cosh \frac{l'}{2} = \sin{A}\cosh \frac{l}{2}
```

(TODO)



###### Поворот с постоянной скоростью

Частный случай предыдущей задачи


```math
a \rightarrow 0 \ \alpha \rightarrow 0, \frac{a}{\alpha}=const
\\
\tanh r = \sinh{\frac{a}{2}} \tan\left(\frac{\pi}{2}-\frac{\alpha}{2}\right) =  \sinh{\frac{a}{2}} \cot{\frac{\alpha}{2}} \approx
 \frac{a}{2} \frac{2}{\alpha} =
 \frac{a}{\alpha} \\
\tanh r = \frac{da}{d\alpha} \\
r= \operatorname{atanh}{\frac{a}{\alpha}}
```
Таким образом, при приближении скорости поворота к 1 справа радиус окружности стремиться к бесконечности.

При скорости поворота равной 1 путник будет ходить по бесконечному кругу(орициклу).

При скорости поворота меньше 1:


TODO


#### ссылка
[калькулятор](https://codepen.io/quant61/pen/ExxRreE?editors=1010)


