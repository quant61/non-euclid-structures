>«Я мамочку часто не слушал, я плохо морковочку кушал, я баиньки поздно ложился, и вот результат — заблудился!»[^1]



Зайчик гулял-гулял и оказался слишком далеко от дома.

Вопрос: насколько легко ему будет добираться обратно.



## Подзадача первая

Итак, зайчик оказался далеко, но у него способность, позволяющая ему узнать направление в сторону дома.

Однако:

1. Зайчик следует направлению с определённой погрешностью
2. Способность нельзя применять слишком часто. Следует свести количество применений к минимуму.

Предположим, реализовался наихудший сценарий и зайчик отклонился на угол $\alpha$. В таком случае зайчик может пройти мимо дома и оказаться дальше, где был.

При движении по прямой наиболее близкой точкой к дому будет пересечение между прямой и перпендикуляром, проведённым из дома.

Следовательно, зайчику, знающему свою погрешность необходимо пройти расстояние до ближайшей точки по наихудшему сценарию.

В этом случае у нас получается прямоугольный треугольник, где прямой угол - наиболее близкая к дому точка, гипотенуза - расстояние от начала до дома, а катетами являются пройденный зайчиком путь и оставшийся путь до дома.

Таким образом нам известны гипотенуза(начальное расстояние до дома) и угол отклонения.

Новое расстояние до дома является катетом, противолежащим начальной точке, а пройденный путь - прилежащим катетом.

### Евклидовый случай

В Евклидовой геометрии всё просто:
```math
r_1 = r_0\sin \alpha \\
l = r_0\cos\alpha \\
k = \frac{r_0-r_1}{l} = r_0\frac{1-\sin\alpha}{\cos\alpha}
```
где `r_0` - начальное расстояние, `r_1` - конечное расстояние, `k` - КПД

Таким образом, даже при отклонении в 30 градусов зайчик на каждом шаге будет ближе к дому в 2 раза. При это сам путь будет больше идеального в $\sqrt3$ раз. Достаточно активировать способность всего 10 раз, чтобы расстояние в 10 км сократить до 10 метров - а там уже и глазами дом видно.

### Геометрия Лобачевского

`c` - расстояние до, `a` - расстояние после, `b` - пройденное расстояние, `A` - угол отклонения.
```math
\sinh{a} = \sinh{c}\sin{A} \\
\tanh{b} = \cos{A}\tanh{c} \\
a = \operatorname{asinh}(\sinh{c}\sin{A}) \\
b = \operatorname{atanh}(\tanh{c}\cos{A}) \\
```

При достаточно большом расстоянии получаем:
```math
e^a=e^c \sin{A} \\
\tanh{b}=\cos{A} \\
e^{a-c} = \sin{A} \\
a-c = \ln{\sin{A}} \\
b = \operatorname{atanh}{\cos A}
```
Таким образом, при отклонении в 1 градус зайчику придётся корректировать курс каждые 4.7 единицы(при этом он будет приближаться к дому на 4.05), при отклонении в 1 минуту - 8.83/8.14, 1 угловую секунду - 12.93/12.23.

В геометрии Лобачевского даже при отклонении в 1 угловую секунду(а этого достаточно, чтобы попасть в монетку с расстояния 3 километров) зайчику придётся поправлять курс каждые 13 радиусов кривизны пространства.

[^1]: скопировано с [posmotre.li](https://posmotre.li/Басня_без_связи#.D0.9C.D1.83.D0.BB.D1.8C.D1.82.D1.84.D0.B8.D0.BB.D1.8C.D0.BC.D1.8B)
